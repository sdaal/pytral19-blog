from django.db import models


class Blog(models.Model):  # generated table name -> posts_blog
    """
    -- The generated SQL is something like the following:
    CREATE TABLE blogs(
      id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
      user VARCHAR(128) NOT NULL,
      title VARCHAR(256) NOT NULL
    );
    """
    user = models.CharField(max_length=128, null=False)
    title = models.CharField(max_length=256, null=False)
    # The models.ForeignKey in the Post class below on line 38 adds the following attribute
    # posts (it is 'posts' because we specified this by using the argument 'related_name')
    # if we had skipped that argument, by default it would be: post_set
    # It allows us to access all posts that belong to this blog


class Post(models.Model):  # generated table name -> posts_post
    """
    -- The generated SQL is something like the following:
    CREATE TABLE posts (
      id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
      title VARCHAR(256) NOT NULL,
      content TEXT NOT NULL,
      is_members_only BOOLEAN NOT NULL DEFAULT FALSE,
      blog_id INTEGER NOT NULL,
      CONSTRAINT fk_posts_blogs_blog_id FOREIGN KEY (blog_id)
        REFERENCES blogs (id)
        ON DELETE CASCADE
    );
    """
    title = models.CharField(max_length=256, null=False)
    content = models.TextField(null=False)
    is_members_only = models.BooleanField(default=False, null=False)
    blog = models.ForeignKey(Blog, on_delete=models.CASCADE, null=False, related_name="posts")
    # blog_id -> the models.ForeignKey above adds this attribute to all instances
