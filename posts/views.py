from django.http import HttpRequest, HttpResponse, HttpResponseNotFound
from django.shortcuts import render

from posts.models import Blog


def get_blog_list(request: HttpRequest) -> HttpResponse:
    """
    Get all available blogs
    """
    blogs = Blog.objects.all()  # Generated SQL: SELECT id, user, title FROM posts_blog
    return render(request, "blog_list.html", context={
        "blogs": blogs,
    })


def get_blog_details(request: HttpRequest, blog_id: int) -> HttpResponse:
    """
    Get details for a specific blog by its ID
    """
    blog = Blog.objects.get(id=blog_id)  # generated SQL: SELECT id, user, title FROM posts_blog WHERE id = ?
    if blog is None:
        return HttpResponseNotFound("<p>Blog does not exist</p>")
    return render(request, "blog_details.html", context={
        "blog": blog,
    })
