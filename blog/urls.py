from django.contrib import admin
from django.urls import path

from posts.views import get_blog_list, get_blog_details
from . import views

urlpatterns = [
    path("admin/", admin.site.urls),
    path("blogs/", get_blog_list),  # the URL would be: http://127.0.0.1:8000/blogs/
    path("blogs/<int:blog_id>/", get_blog_details),  # The URL would be: http://127.0.0.1:8000/blogs/123/
    path("", views.home, name="home"),
]
