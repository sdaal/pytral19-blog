Python Tirana AL 19
===================

Django Example Project
----------------------

### Prerequisites

- Git installed and accessible from the command line (check by running `git --version`)
- Python installed and accessible from the command line (check by running `python --version`)

### Getting Started

Open your shell of choice in a terminal window (e.g. PowerShell on Windows).
Navigate to the directory where you keep your projects.
Let's assume your username is `jlennon` and you are on Windows and you keep your projects in `C:\Users\jlennon\Projects`.

```commandline
cd C:\Users\jlennon\Projects
git clone https://gitlab.com/sdaal/pytral19-blog.git
cd pytral19-blog
```

Now you should be inside the projects directory.
First you need to create a virtual environment and install the required dependencies.

```commandline
python -m venv --prompt=blog venv
venv\Scripts\activate
pip install -r requirements.txt
```

At this point you should be able to apply the migrations in order to create the database tables.

```commandline
python manage.py migrate
```

